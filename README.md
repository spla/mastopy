# masto.py
Mastodon client written in Python

### Dependencies

-   **Python 3**
-   Everything else at the top of `masto.py`!

### Usage:

Install masto.py, create & activate the Python Virtual Environment:

1. git clone https://gitlab.com/spla/mastopy.git 'yourdirectory'
2. python3 -m venv 'yourdirectory'
3. cd 'yourdirectory'
4. source bin/activate

Within already activated Python Virtual Environment:

1. Run ' python setup.py' to get your Mastodon's access token of an existing user. It will be saved to 'secrets/secrets.txt' for further use.

2. Run 'python masto.py' 

Note: install all needed packages with 'pip install package' or use 'pip install -r requirements.txt' to install them.

### New feature!

11.12.19 - Added reblogs, favourites and replies support!  
17.12.19 - Added keys help window!  
17.12.19 - New interaction window!  
17.12.19 - Added server information window!  
18.12.19 - Added logged in user's profile information window!  
19.12.19 - Added profile view to interaction window!
