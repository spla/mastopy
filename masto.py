from html.parser import HTMLParser
import pdb
import curses
from curses.textpad import Textbox, rectangle
from curses import wrapper, textpad
from mastodon import Mastodon
import os
import sys
from datetime import datetime, timezone, timedelta

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

def compare_strings_len(s1, s2):
    if len(s1) > len(s2):
        return s1
    elif len(s1) < len(s2):
        return s2
    else:
        return None

def toot_input(toot_id, acct_id):

    curses.curs_set(1)
    inp = curses.newwin(8,65, 10,25)
    inp.border()
    if acct_id != "":
      inp.addstr(1,2, "What must you say to " + acct_id + " ?")
    else:
      inp.addstr(1,2, "What must you say?")
    sub = inp.subwin(3, 63, 12, 26)
    sub.border()
    sub2 = sub.subwin(1, 61, 13, 27)
    tb = curses.textpad.Textbox(sub2)
    inp.refresh()
    tb.edit()
    toot = tb.gather()
    curses.curs_set(0)
    return toot

def toot_win(toot_number, toot_id, win_name, win_begin_y, win_begin_x, user, account, created, content, media, retoot, reply_id):

    win_name = curses.newwin(7, curses.COLS-2, win_begin_y, win_begin_x)
    if retoot == None and reply_id == None:
      if user != None:
        win_name.addstr(1,2, user + " (" + account + ")")
      else:
        win_name.addstr(1,2, account)
    else:
      win_name.addstr(1,2, user)
    win_name.addstr(1,int(curses.COLS/2)+int(len(created)/2), created)
    win_name.addstr(1, curses.COLS-5, str(toot_number))
    content = (content[:422] + '... ') if len(content) > 422 else content
    win_name.addstr(3,2, content)
    if media != []:
      win_name.addstr(4,2, media)
    win_name.border()
    win_name.refresh()

def profile_win(stdscr, height, width, k, profile):

    k = 0
    exit_str = "m or p to close"
    title_str = "profile information"
    user_str = "username: " + profile['username']
    acct_str = "account: " + profile['acct']
    created = profile['created_at']
    created = created.strftime("%d/%m/%Y, %H:%M:%S") 
    created_str = "created_at: " + str(created)
    following_str = "following: " + str(profile['following_count'])
    followers_str = "followers: " + str(profile['followers_count'])
    status_str = "statuses: " + str(profile['statuses_count'])
    win_height = 12
    larger_str = compare_strings_len(acct_str, created_str)
    if larger_str != None:
      win_width = len(larger_str)+4
      exit_str = exit_str.center(len(larger_str)-4)
      title_str = title_str.center(len(larger_str))
    else:
      win_width = len(acct_str)+4
      exit_str = exit_str.center(len(created_str)-4)
      title_str = title_str.center(len(created_str))
    profile_win_name = curses.newwin(win_height, win_width, int(height/2)-int(win_height/2), int(width/2)-int(win_width/2))
    profile_win_name.addstr(1,2, title_str)
    profile_win_name.addstr(3,2, user_str)
    profile_win_name.addstr(4,2, acct_str)
    profile_win_name.addstr(5,2, created_str)
    profile_win_name.addstr(6,2, following_str)
    profile_win_name.addstr(7,2, followers_str)
    profile_win_name.addstr(8,2, status_str)
    profile_win_name.addstr(10,2, exit_str)
    profile_win_name.border()
    profile_win_name.refresh()
    while True:

      if k == ord('m') or k == ord('p'):

        k = 0
        profile_win_name.clear()
        break

      k = stdscr.getch()

def server_win(stdscr, height, width, k):

    k = 0
    info = mastodon.instance()
    title_str = info['uri'] + " information"
    version_str = "version: " + info['version']
    user_stats = "users: " + str(info['stats']['user_count'])
    status_stats = "statuses: " + str(info['stats']['status_count'])
    domains_stats = "domains: " + str(info['stats']['domain_count'])
    registrations = "registration opened: " + str(info['registrations'])
    approval = "approval required: " + str(info['approval_required']) 
    exit_str = "i to close"

    win_height = 15
    win_width = len(registrations)+4
    exit_str = exit_str.center(len(registrations)-4)
    server_win_name = curses.newwin(win_height, win_width, int(height/2)-int(win_height/2), int(width/2)-int(win_width/2))
    server_win_name.addstr(1,2, title_str)
    server_win_name.addstr(3,2, version_str)
    server_win_name.addstr(5,2, "stats")
    server_win_name.addstr(6,2, user_stats)
    server_win_name.addstr(7,2, status_stats)
    server_win_name.addstr(8,2, domains_stats)
    server_win_name.addstr(10,2, registrations)
    server_win_name.addstr(11,2, approval)
    server_win_name.addstr(13,2, exit_str)
    server_win_name.border()
    server_win_name.refresh()
    while True:

      if k == ord('i'):

        k = 0
        server_win_name.clear()
        break
      k = stdscr.getch()

def help_win(stdscr, height, width, k):

    k = 0
    title_str = "* masto.py keys *"
    toot_str = "- t to toot"
    refresh_str = "- r to refresh current timeline" 
    timelines_str = "- h, l or p to cycle timelines"
    timelines2_str = "     (home, local or public)"
    notif_str = "- n to get notifications"
    prof_str = "- m to view your profile"
    info_str = "- i to server information"
    toot_number_str = "- press toot number to interact"  
    subtitle_str = "Interactions"
    interact_str = "- b, f or r for boost, fav or reply" 
    interact2_str = "- p to see user's profile"
    exit_str = "- k to close this help"
    win_height = 18
    win_width = len(interact_str)+4
    title_str = title_str.center(win_width-4)
    help_win_name = curses.newwin(win_height, win_width, int(height/2)-int(win_height/2), int(width/2)-int(win_width/2))
    help_win_name.addstr(1,2, title_str)
    help_win_name.addstr(3,2, toot_str)
    help_win_name.addstr(4,2, refresh_str)
    help_win_name.addstr(5,2, timelines_str)
    help_win_name.addstr(6,2, timelines2_str)
    help_win_name.addstr(7,2, notif_str)
    help_win_name.addstr(8,2, prof_str)
    help_win_name.addstr(9,2, info_str)
    help_win_name.addstr(10,2, exit_str)
    help_win_name.addstr(12,2, subtitle_str)
    help_win_name.addstr(14,2, toot_number_str)
    help_win_name.addstr(15,2, interact_str)
    help_win_name.addstr(16,2, interact2_str)
    help_win_name.border()
    help_win_name.refresh()
    while True:

      if k == ord('k'):

        k = 0
        help_win_name.clear()
        break
      k = stdscr.getch()

def interact(stdscr, height, width, interact_id, i, user_id):

  interact_win_name = interact_win(interact_id)
  interact_inp = interact_win_name

  k = 0

  boosted_by = len(mastodon.status_reblogged_by(interact_id))
  fav_by = len(mastodon.status_favourited_by(interact_id))

  if boosted_by != 0:

      boosted_by_str = "Boosted by: " + str(boosted_by)

  if fav_by != 0:

      fav_by_str = "Favourited by: " + str(fav_by)

  choose_str = "- b to boost, f to favourite, r to reply | p to see user's profile"
  exit_str = "- q to close this interaction window"

  win_height = 8
  win_width = len(choose_str)+4
  win_pos_y = interact_win_name.getmaxyx()[0]-7
  win_pos_x = int(interact_win_name.getmaxyx()[1]/2)-int(win_width/2)
  choose_str = choose_str.center(len(choose_str)-4)
  
  interact_sub = interact_inp.subwin(7, win_width, win_pos_y, win_pos_x)
  interact_sub.border()

  if boosted_by != 0:

    interact_sub.addstr(1,2, boosted_by_str)
  
  if fav_by != 0:
 
   interact_sub.addstr(2,2, fav_by_str)

  interact_sub.addstr(3,2, choose_str)
  interact_sub.addstr(4,2, exit_str)
  interact_sub.refresh()

  k = stdscr.getch()

  while True:

    if k == ord('b'):

      tmline = 'boost'
      mastodon.status_reblog(interact_id)
      k = 0
      interact_inp.clear()
      break 

    if k == ord('f'):

      tmline = 'favourite'
      mastodon.status_favourite(interact_id)
      k = 0
      interact_inp.clear()
      break

    if k == ord('p'):

      interact_sub.clear()
      interact_sub.refresh()     
      profile = mastodon.account(user_id)
      profile_win(stdscr, height, width, k, profile)
      k = 0
      interact_inp.clear()
      break

    if k == ord('r'):

      reply_toot = mastodon.status(interact_id)
      toot_id = reply_toot['id']
      acct_id =	reply_toot['account']['acct']
      toot_vis = reply_toot['visibility']

      toot = toot_input(toot_id, acct_id)

      if toot != "":
        toot = "@" + acct_id + " " + toot
        mastodon.status_post(toot, in_reply_to_id=toot_id, visibility=toot_vis)

      k = 0
      interact_inp.clear()
      stdscr.clear()
      break

    if k == ord('q'):

      k = 0
      interact_inp.clear()
      stdscr.clear()
      break

def interact_win(interact_id):

   interact_win_begin_y = 1
   interact_win_begin_x = 1
   interact_win_name = "interaction"
   title_str = "Interaction window"
   title_len = len(title_str)
   user = str(mastodon.status(interact_id)['account']['display_name'])
   created = mastodon.status(interact_id)['created_at'] + timedelta(hours=1)
   created = created.strftime("%d/%m/%Y, %H:%M:%S")
   content = strip_tags(mastodon.status(interact_id)['content'])

   interact_win_name = curses.newwin(16, curses.COLS-2, interact_win_begin_y, interact_win_begin_x)

   interact_win_name.addstr(1,2, user)
   interact_win_name.addstr(1,30, created)
   interact_win_name.addstr(1, curses.COLS-title_len-4, title_str)
   interact_win_name.addstr(3,2, content)
   #if media != []:
    # win_name.addstr(2,2, media)
   interact_win_name.border()
   interact_win_name.refresh()
   return interact_win_name

def main(stdscr):

  curses.curs_set(0)
  k = 0
  cursor_x = 0
  cursor_y = 0
  curses.noecho()
  stdscr.keypad(True)

  curses.start_color()
  curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
  curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
  curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

  tmline = 'home'
  home = mastodon.timeline(timeline=tmline)
  remaining_api_requests = mastodon.ratelimit_remaining

  while True:

    if k == ord('t'):

      toot_id = ""
      acct_id = ""

      toot = toot_input(toot_id, acct_id)

      if toot != "":
        mastodon.status_post(toot, in_reply_to_id=None, visibility="private")
      home = mastodon.timeline(timeline=tmline)
      k = 0
      stdscr.clear()
      
    if k == ord('r'): 

      if tmline != 'notifications':
        home = mastodon.timeline(timeline=tmline)
        remaining_api_requests = mastodon.ratelimit_remaining
      else:
        home = mastodon.notifications()
      remaining_api_requests = mastodon.ratelimit_remaining

      k = 0
      i = 0     
      stdscr.clear()

    if k == ord('n'):

      tmline = 'notifications'
      k = 0
      #stdscr.clear()

    if k == ord('h'):

      tmline = 'home'
      home = mastodon.timeline(timeline=tmline)
      remaining_api_requests = mastodon.ratelimit_remaining
      k = 0
      i = 0
      stdscr.clear()

    if k == ord('l'):

      tmline = 'local'
      home = mastodon.timeline(timeline=tmline)
      remaining_api_requests = mastodon.ratelimit_remaining
      k = 0
      i = 0
      stdscr.clear()

    if k == ord('p'):

      tmline = 'public'
      home = mastodon.timeline(timeline=tmline)
      remaining_api_requests = mastodon.ratelimit_remaining
      k = 0
      i = 0
      stdscr.clear()

    if k == ord('m'):

      profile = mastodon.account_verify_credentials()
      profile_win(stdscr, height, width, k, profile)
      k = 0

    if k == ord('0') or k == ord('1') or k == ord('2') or k == ord('3'):

      if k == ord('0'):
        if tmline != "notifications":
          interact_id = home[0]['id']
          user_id = home[0]['account']['id']
        else:
          interact_id = notif[0]['status']['id'] 
          user_id = notif[0]['account']['id']
      if k == ord('1'):
        if tmline != "notifications":
          interact_id = home[1]['id']
          user_id = home[1]['account']['id']
        else:
          interact_id = notif[1]['status']['id']
          user_id = notif[1]['account']['id']
      if k == ord('2'):
        if tmline != "notifications":
          interact_id = home[2]['id']
          user_id = home[2]['account']['id']
        else:
          interact_id = notif[2]['status']['id']
          user_id = notif[2]['account']['id']
      if k == ord('3'):
        if tmline != "notifications":
          interact_id = home[3]['id']
          user_id = home[3]['account']['id'] 
        else:
          interact_id = notif[3]['status']['id']
          user_id = notif[3]['account']['id']
      
      interact(stdscr, height, width, interact_id, i, user_id)
      k = 0

    if k == ord('k'):

      help_win(stdscr, height, width, k)

    if k == ord('i'):

      server_win(stdscr, height, width, k)

    if k == ord('q'):
     
      sys.exit("Bye")
 
    while True:

      stdscr.refresh()

      height, width = stdscr.getmaxyx()
      topbar_y, topbar_x = stdscr.getbegyx()

      cursor_x = max(0, cursor_x)
      cursor_x = min(width-1, cursor_x)

      cursor_y = max(0, cursor_y)
      cursor_y = min(height-1, cursor_y)

      topbarstr = " Server: " + str(mastodon_hostname) + " | timeline : " + str(tmline) + " | Remaining API requests: " + str(remaining_api_requests) + " | Lines: " + str(curses.LINES) + " Columns: " + str(curses.COLS).format(topbar_x, topbar_y)

      # Render top bar
      stdscr.attron(curses.color_pair(3))
      stdscr.addstr(topbar_x, 0, topbarstr)
      stdscr.addstr(topbar_x, len(topbarstr), " " * (width - len(topbarstr) - 1))
      stdscr.attroff(curses.color_pair(3))

      statusbarstr = " Press 'q' to exit | 'k' for keys help | spla@mastodont.cat - 2019".format(cursor_x, cursor_y)

      # Render status bar
      stdscr.attron(curses.color_pair(3))
      stdscr.addstr(height-1, 0, statusbarstr)
      stdscr.addstr(height-1, len(statusbarstr), " " * (width - len(statusbarstr) - 1))
      stdscr.attroff(curses.color_pair(3))

      i = 0
      while i < 4:

        if tmline != 'notifications':

          toot_id = home[i]['id']
          if home[i]['account']['display_name'] != None:

            user = home[i]['account']['display_name']

          else:

            user = home[i]['account']['username']

          account = home[i]['account']['acct']
          created = home[i]['created_at'] + timedelta(hours=1)
          created = created.strftime("%d/%m/%Y, %H:%M:%S")
          content = strip_tags(home[i]['content'])
          media = home[i]['media_attachments']
          if media != []:
            if media[0]['remote_url'] != None:
              media = media[0]['type'] + " | " + media[0]['remote_url']
            else:
              media = media[0]['type'] + " | " + media[0]['preview_url']

          retoot = home[i]['reblog']
          reply_id = home[i]['in_reply_to_account_id']

          if retoot != None:

            boosted_user= home[i]['reblog']['account']['acct']
            user = str(home[i]['account']['acct']) + " boosted " + str(boosted_user)
            content = strip_tags(home[i]['reblog']['content'])

          if reply_id != None:

            replied_user= mastodon.account(reply_id)['acct']
            user = str(home[i]['account']['acct']) + " replied " + str(replied_user)
            content = strip_tags(home[i]['content'])

          if i == 0:

            win_begin_y = 1 
            win_begin_x = 1
            toot_number = 0
            win_name = "toot_zero_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

          elif i == 1:
 
            win_begin_y = 1
            win_begin_x = 8
            toot_number = 1
            win_name = "toot_one_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

          elif i == 2:

            win_begin_y = 1
            win_begin_x = 15
            toot_number = 2
            win_name = "toot_two_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

          elif i == 3:

            win_begin_y = 1
            win_begin_x = 22
            toot_number = 3 
            win_name = "toot_three_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

        else: # notifications

          notif = mastodon.notifications()
          remaining_api_requests = mastodon.ratelimit_remaining

          toot_type = notif[i]['type']

          if toot_type == 'follow':
            user = str(notif[i]['account']['acct']) + " now follows you"
            account = ""
            created = notif[i]['created_at'] + timedelta(hours=1)
            created = created.strftime("%d/%m/%Y, %H:%M:%S")
            content = ""
            media = ""
          if toot_type == 'mention':
            user = str(notif[i]['account']['acct']) + " mentioned you"
            account = ""
            created = notif[i]['created_at'] + timedelta(hours=1)
            created = created.strftime("%d/%m/%Y, %H:%M:%S")
            content = strip_tags(notif[i]['status']['content'])
            media = notif[i]['status']['media_attachments']
            if media != []:
              if media[0]['remote_url'] != None:
                media = media[0]['type'] + " | " + media[0]['remote_url']
              else:
       	        media = media[0]['type'] + " | " + media[0]['preview_url']
          if toot_type == 'reblog':
            user = str(notif[i]['account']['acct']) + " reblogged you"
            account = ""
            created = notif[i]['created_at'] + timedelta(hours=1)
            created = created.strftime("%d/%m/%Y, %H:%M:%S")
            content = strip_tags(notif[i]['status']['content'])
            media = notif[i]['status']['media_attachments']
            if media != []:
              if media[0]['remote_url'] != None:
                media = media[0]['type'] + " | " + media[0]['remote_url']
              else:
                media = media[0]['type'] + " | " + media[0]['preview_url']
          if toot_type == 'favourite':
            user = str(notif[i]['account']['acct']) + " favorited you"
            account = ""
            created = notif[i]['created_at'] + timedelta(hours=1)
            created = created.strftime("%d/%m/%Y, %H:%M:%S")
            content = strip_tags(notif[i]['status']['content'])
            media = notif[i]['status']['media_attachments']
            if media != []:
              if media[0]['remote_url'] != None:
                media = media[0]['type'] + " | " + media[0]['remote_url']
              else:
                media = media[0]['type'] + " | " + media[0]['preview_url'] 

          if i == 0:

            win_begin_y = 1
            win_begin_x = 1
            toot_number = 0
            if toot_type != 'follow':
              toot_id = notif[0]['status']['id']
            win_name = "toot_zero_win" 
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

          elif i == 1:

            win_begin_y = 1
            win_begin_x = 8
            toot_number = 1
            if toot_type != 'follow':
              toot_id = notif[1]['status']['id']
            win_name = "toot_one_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

          elif i == 2:

            win_begin_y = 1
            win_begin_x = 15
            toot_number = 2
            if toot_type != 'follow':
              toot_id = notif[2]['status']['id']
            win_name = "toot_two_win"
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)
 
          elif i == 3:

            win_begin_y = 1
            win_begin_x = 22
            toot_number = 3
            if toot_type != 'follow':
              toot_id = notif[3]['status']['id']
            win_name = "toot_three_win" 
            toot_win(toot_number, toot_id, win_name, win_begin_x, win_begin_y, user, account, created, content, media, retoot, reply_id)

        i += 1

      # Refresh the screen
      stdscr.refresh()

      k = stdscr.getch()

      if k == '0':

        remaining_api_requests = mastodon.ratelimit_remaining
              
      if k == ord('i'):
        break
      if k == ord('k'):
        break
      if k == ord('n'):
        break
      if k == ord('h'):
        break
      elif k == ord('l'):
        break
      if k == ord('p'):
        break
      elif k == ord('r'):
        break
      if k == ord('q'):
        break
      if k == ord('t'):
        break
      if k == ord('b'):
        break
      if k == ord('m'):
        break
      if k == ord('0') or k == ord('1') or k == ord('2') or k == ord('3'):
        break
      
  curses.nocbreak()
  stdscr.keypad(True)
  curses.echo()
  curses.endwin()

###############################################################################
# INITIALISATION
###############################################################################

# Returns the parameter from the specified file
def get_parameter( parameter, file_path ):
    # Check if secrets file exists
    if not os.path.isfile(file_path):
        print("File %s not found, exiting."%file_path)
        sys.exit(0)

    # Find parameter in file
    with open( file_path ) as f:
        for line in f:
            if line.startswith( parameter ):
                return line.replace(parameter + ":", "").strip()

    # Cannot find parameter, exit
    print(file_path + "  Missing parameter %s "%parameter)
    sys.exit(0)

# Load secrets from secrets file
secrets_filepath = "secrets/secrets.txt"
uc_client_id     = get_parameter("uc_client_id",     secrets_filepath)
uc_client_secret = get_parameter("uc_client_secret", secrets_filepath)
uc_access_token  = get_parameter("uc_access_token",  secrets_filepath)

# Load configuration from config file
config_filepath = "config.txt"
mastodon_hostname = get_parameter("mastodon_hostname", config_filepath) # E.g., mastodon.social

# Initialise Mastodon API
mastodon = Mastodon(
    client_id = uc_client_id,
    client_secret = uc_client_secret,
    access_token = uc_access_token,
    api_base_url = 'https://' + mastodon_hostname,
)

# Initialise access headers
headers={ 'Authorization': 'Bearer %s'%uc_access_token }

###############################################################

curses.wrapper(main)
